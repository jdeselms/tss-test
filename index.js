const axios = require('axios')
const { getAllPermutationsOfPartialStates } = require('./permutation')
const allfs = require('fs')
const _ = require('lodash')
const { get } = require('lodash')

const fs = allfs.promises
fs.existsSync = allfs.existsSync

const PARALLELIZATION = 10

async function getMpvs() {
    return await getFileOrHttp("https://merchandising-product-service.cdn.vpsvc.com/api/v3/mpv/vistaprint?requestor=wrangler-mangler-tss", "mpvs.json")
}

async function getMerchandisedOptions(mpvId, locale) {
    const url = `https://merchandising-product-service.cdn.vpsvc.com/api/v3/mpv/vistaprint/${locale}/${mpvId}?requestor=wrangler-mangler-tss`
    const response = await getFileOrHttp(url, `MPV-${mpvId}-${locale}.json`)
    if (!response) {
//        console.log(`*** MPV ${mpvId} in ${locale} NOT FOUND ***`)
        return undefined
    }

    if (response.ppagTail) {
//        console.log(`*** MPV ${mpvId} in ${locale} is ppag. skipping.`)
        return undefined
    }
    
    // Now, get the set of options that are on any experience (except Summary)
    const allMerchandisedOptions = _.chain(response.merchandisingDetails)
        .filter(md => md.type !== "Summary")
        .map(md => md.properties.map(p => p.key))
        .flatten()
        .uniq()
        .sort()
        .value()

    return allMerchandisedOptions
}

async function getProduct(productKey) {
    const response = await getFileOrHttp(`https://catalog.products.vpsvc.com/api/products/${productKey}?requestor=wrangler-mangler-tss`, `${productKey}.json`)
    if (!response) {
        return undefined
    }

    const result = {}
    for (const option of response.options.list) {
        result[option.name] = option.values
    }

    return result
}

let total = 0
const start = Date.now()

async function processProducts(products, prdToMpv) {
    for (const product of products) {
        await processProduct(product, prdToMpv)
    }
}

async function processProduct(productKey, prdToMpv) {
    const product = await getProduct(productKey)
    if (!product) {
        console.error(`Can't get product ${productKey}`)
        return
    }
    const localesAndMpvs = prdToMpv[productKey]

    let totalForProduct = 0

    const entries = Object.entries(localesAndMpvs)

    for (const [locale, mpvId] of entries) {
        const merchandisedOptions = await getMerchandisedOptions(mpvId, locale)
        if (!merchandisedOptions) {
            continue
        }

        const permutations = getAllPermutationsOfPartialStates(merchandisedOptions, product)
        totalForProduct += permutations.length
        total += permutations.length

        for (const permutation of permutations) {
            const response = await callTss(productKey, permutation, locale)
//            console.log(`${productKey}/${locale}`, toKey(permutation), response)
        }
    }

//    console.log(`${productKey}: ${entries.length} locales, ${totalForProduct} permutations`)
    const now = Date.now()
    const duration = now - start

    if (totalForProduct > 0) {
        console.log(`Total: ${total}, ms: ${duration}, rate: ${total/duration * 1000}/sec`)
    } else {
        const mpvs = Array.from(new Set(Object.values(prdToMpv[productKey])))
            .join(', ')

//        console.log(`Skipped product ${productKey}, mpv ${mpvs}`)
    }
}

function toKey(obj) {
    const result = Object.entries(obj).map(([key, value]) => `${formatString(key)},${formatString(value)}`)

    return result.join('|')
}

function formatString(str) {
    return encodeURIComponent(str.replace(/\//g, "{SLASH}"))
}

async function callTss(productKey, options, culture) {
    await delay(100)

    const baseUrl = "https://targetspecifications.designtransformation.vpsvc.com/api/v3/productSpecifications"

    let url = `${baseUrl}/${productKey}?culture=${culture}`
    let filename = `tss/${productKey}-${culture}-`
    const params = new URLSearchParams()
    params.append("culture", culture)

    for (let [key, value] of Object.entries(options))
    {
        url += `&optionSelections[${encodeURIComponent(key)}]=${encodeURIComponent(value)}`
    }

    filename += toKey(options) + ".json"

    try {
        if (filename === 'tss/PRD-UAGOO5YW-de-AT-Corner,Rounded|Foil Color,None|Fold,Folded|Product Orientation,Vertical|Size,4 x 8in|Stock,C2S Silk (EU) / Uncoated Matte (NA) 14pt.json') {
            return await getFileOrHttp(url, filename, [400, 404])
        }
        return await getFileOrHttp(url, filename, [400, 404])
    } catch (err) {
        console.error(err, url)
    }
}

function splitIntoGroups(array, chunks) {
    return _.chain(array)
        .map((x, i) => [x, i % chunks])
        .groupBy(([_, i]) => i)
        .mapValues(arr => arr.map(([x, i]) => x))
        .values()
        .value()
}

async function run() {
    const allMpvs = await getMpvs()
    const prdToMpv = mapPrdsToMpvs(allMpvs)
    const allProductKeys = Object.keys(prdToMpv)
//        .filter(k => k === "PRD-HGL2NPXJ") // TODO

    // const products = allProductKeys
    //     .filter(k => k === 'PRD-RWLCHPA0')

    console.log(`Products to process: ${allProductKeys.length}`)

    const groups = splitIntoGroups(allProductKeys, PARALLELIZATION)

    await Promise.all(groups.map(g => processProducts(g, prdToMpv)))

    console.log("TOTAL", total)
}

function mapPrdsToMpvs(mpvs) {
    const map = {}

    for (const mpv of mpvs) {
        const productKeys = _.chain(mpv.products)
            .entries()
            .value()

        for (const [locale, productKey] of productKeys) {
            const prd = map[productKey] || (map[productKey] = {}, map[productKey])
            prd[locale] = mpv.mpvId
        }
    } 

    return map
}

async function getFileOrHttp(url, filename, allowedStatues=[404]) {
    const fullName = `/Users/jdeselms/temp/products/` + filename

    if (fs.existsSync(fullName)) {
        const text = await fs.readFile(fullName, 'utf-8')
        if (text && text !== '') {
            return JSON.parse(text)
        } else { 
            return undefined
        }
    }

    // Get it and save it for next time.
    let result;
    try {
        result = await getWithThrottle(url)
        if (!result || allowedStatues.includes(result.status)) {
            console.log(result.status, url)
            return undefined
        }
        await fs.writeFile(fullName, JSON.stringify(result.data), 'utf-8')
        return result.data
    } catch (err) {
        if (allowedStatues.includes(err.response.status)) {
            await fs.writeFile(fullName, '', 'utf-8')
            return undefined
        }
        console.error(err)
        throw err
    }
}

async function getWithThrottle(url) {
    try {
        return await axios.get(url)
    } catch (err) {
        if (err.response?.status === 503) {

            console.error("Waiting")

            // If we're overwhelming the service, then let's wait a bit and try again.
            const DELAY_MS = 5000

            await new delay(DELAY_MS)

            console.error("Resuming")

            try {
                return await axios.get(url)
            } catch (err) {
                console.error("*** FAILED AFTER RETRY")
            }
        } else {
            throw err
        }
    }
}

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

run().catch(console.error)

// const array = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']
// console.log(splitIntoGroups(array, 3))