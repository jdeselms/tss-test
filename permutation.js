const _ = require('lodash')

/**
 * Combines all the permutations of partial states into one list of configurations
 * @param {[String]} merchandisedOptions a list of merchandised option names
 * @param {ProductModel} productModel the product model as defined by catalog.products.vpsvc.com
 */
function getAllPermutationsOfPartialStates(merchandisedOptions, productModel) {
  let merchOptionStates = merchandisedOptions.sort().map(option => {
    const optionValues = productModel[option]
    return optionValues.map((value) => {
      return {
        [option]: value
      };
    });
  });

  // returns [{key:value, {key2:value2},...]
  const allPartialConfigurationStates = permute(merchOptionStates);
  allPartialConfigurationStates.push({})

  return allPartialConfigurationStates;
}

function permute(list) {
    const permutations = permuteAll(...list)
    const result = permutations.map(nestedArrayOfOptions => {
        return nestedArrayOfOptions.reduce((previous, current) => {
          return Object.assign(previous, current);
        }, {});
    })
    return result
}

function permuteAll(first, ...rest) {
    let ret = [];
    if (first && first.length) {
      // Walk the first branch (where all nodes are prefixed by elements in the first array)
      for (let firstArrayElement of first) {
        ret.push([firstArrayElement])
        // The selection with the rest of the permutations
        for (let permutation of permuteAll(...rest)) {
          ret.push([].concat(firstArrayElement, permutation))
        }
      }
      // Walk the second branch where the first array elements don't exist at all
      for (let permutation of permuteAll(...rest)) {
        ret.push(permutation)
      }
    }
  
    return ret;
  }
  
  module.exports = {
    getAllPermutationsOfPartialStates
  }
  